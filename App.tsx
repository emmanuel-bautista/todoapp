import React, { useEffect, useState } from 'react';
import {LogBox, StatusBar, useColorScheme} from 'react-native';
import {LightTheme} from './src/theme/appTheme';
import {Navigator} from './src/navigation/Navigator';
import {TaskProvider} from './src/context/task/TaskContext';
import {NavigationContainer} from '@react-navigation/native';

const AppState = ({children}: {children: JSX.Element | JSX.Element[]}) => {
  return <TaskProvider>{children}</TaskProvider>;
};

export const App = () => {
  LogBox.ignoreLogs([
    "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
  ]);

  const colorScheme = useColorScheme();

  const [theme, setTheme] = useState<'dark' | 'light'>(colorScheme === 'light' ? 'light' : 'dark');
  
  useEffect(() => {
    colorScheme === 'light' ? setTheme('light') : setTheme('dark')
  }, [colorScheme]);
  

  return (
    <NavigationContainer theme={LightTheme}>
      <AppState>
        <StatusBar
          backgroundColor={ theme === 'light' ? 'white' : 'black'  }
          barStyle={theme === 'light' ? 'dark-content' : 'light-content'}
        />
        <Navigator />
      </AppState>
    </NavigationContainer>
  );
};
