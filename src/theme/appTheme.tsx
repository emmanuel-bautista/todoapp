import {Theme} from '@react-navigation/native';
import {StyleSheet} from 'react-native';

export const LightTheme: Theme = {
  dark: false,
  colors: {
    primary: 'rgb(6,106,255)',
    background: 'white',
    card: 'white',
    text: 'black',
    border: 'white',
    notification: 'white',
  },
};

export const globalStyles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  title: {
    fontSize: 44,
    fontWeight: 'bold',
    marginVertical: 10
  },
});
