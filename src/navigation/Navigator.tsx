import React from 'react';
import {HomeScreen} from '../screens/HomeScreen';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {TaskScreen} from '../screens/TaskScreen';
import {Task} from '../interfaces/appInterfaces';

export type RootStackProps = {
  Home: undefined;
  Task: Task | undefined;
};

const Stack = createStackNavigator<RootStackProps>();

export const Navigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen
        name="Task"
        component={TaskScreen}
        options={{
          title: '',
          headerShown: true,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontSize: 30,
            fontWeight: 'bold',
          },
        }}
      />
    </Stack.Navigator>
  );
};
