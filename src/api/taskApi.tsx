import axios from "axios";

const taskApi = axios.create({
    baseURL: 'http://pwa-api.nuvem.mx/api'
});

export default taskApi;