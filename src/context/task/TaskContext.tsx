import {Task} from '../../interfaces/appInterfaces';
import React, {createContext, useEffect, useState} from 'react';
import taskApi from '../../api/taskApi';

type TaskContextProps = {
  tasks: Task[];
  error: string | null;
  isLoading: boolean;
  getTasks: () => Promise<void>;
  updateTask: (task: Task) => Promise<void>;
  createTask: (task: Task) => Promise<void>;
  deleteTask: (id: number) => Promise<void>;
  clearError: () => void;
};

export const TaskContext = createContext({} as TaskContextProps);

export const TaskProvider = ({children}: any) => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [error, setError] = useState<string | null>(null);
  const [isLoading, setisLoading] = useState(false);

  useEffect(() => {
    getTasks();
  }, []);

  const getTasks = async () => {
    setisLoading(true);
    const res = await taskApi.get<Task[]>('/tasks');
    setTasks(res.data);
    setisLoading(false);
  };

  const updateTask = async (task: Task) => {
    try {
      setError(null);
      setisLoading(true);
      const res = await taskApi.post<Task>(
        `/tasks/${task.id}`,
        {name: task.name, description: task.description, status: task.status, _method: 'PUT'},
        {
          headers: {
            Accept: 'application/json',
          },
        },
      );

      setTasks([
        ...tasks.map(t => {
          return t.id === res.data.id ? res.data : t;
        }),
      ]);
      setisLoading(false);
    } catch (error) {
      setisLoading(false);
      setError('Ha ocurrido un error');
    }
  };

  const createTask = async (task: Task) => {
    try {
      setError(null);
      setisLoading(true);
      const res = await taskApi.post<Task>(
        '/tasks',
        {name: task.name, description: task.description, status: task.status},
        {
          headers: {
            Accept: 'application/json',
          },
        },
      );

      setTasks([...tasks, res.data]);
      setisLoading(false);
    } catch (error) {
      setisLoading(false);
      setError('Ha ocurrido un error');
    }
  };

  const deleteTask = async (id: number) => {
    try {
      setError(null);
      const res = await taskApi.delete<Task>(`/tasks/${id}`);

      setTasks([
        ...tasks.map(t => {
          return t.id === res.data.id ? res.data : t;
        }),
      ]);
    } catch (error) {
      setError('Ha ocurrido un error');
    }
  };

  const clearError = () => setError(null);

  return (
    <TaskContext.Provider
      value={{
        tasks,
        error,
        isLoading,
        getTasks,
        updateTask,
        createTask,
        deleteTask,
        clearError,
      }}>
      {children}
    </TaskContext.Provider>
  );
};
