import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  Keyboard,
  Alert,
} from 'react-native';
import React, {useContext, useEffect, useState} from 'react';
import {globalStyles, LightTheme} from '../theme/appTheme';
import {StackScreenProps} from '@react-navigation/stack';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {RootStackProps} from '../navigation/Navigator';
import {useForm} from '../hooks/useForm';
import {TaskContext} from '../context/task/TaskContext';

interface Props extends StackScreenProps<RootStackProps, 'Task'> {}

export const TaskScreen = ({route, navigation}: Props) => {
  const task = route.params;

  const {createTask, updateTask,error, clearError} = useContext(TaskContext);
  const [isSubmiting, setIsSubmiting] = useState(false);

  const {name, description, onChange} = useForm({
    name: task?.name || '',
    description: task?.description || '',
  });
  

  useEffect(() => {
    if (!error || error.length === 0) return;
    Alert.alert('Error', error, [
      {
        text: 'Ok',
        onPress: clearError
      }
    ]); 
  }, [error]);
  

  const submit = async () => {
    Keyboard.dismiss();
    if (name.trim() !== '' && description.trim() !== '') {
      setIsSubmiting(true);
      if (task) {
        await updateTask({
          ...task,
          name,
          description
        });
      }  else {
        await createTask({
          id: 1,
          name,
          description,
          status: 1
        });
      }
      setIsSubmiting(false);
      navigation.replace('Home');
    } else {
      Alert.alert('Error', 'Ambos campos son obligatorios');
    }
  };
  
  return (
    <View style={{...globalStyles.container, flex: 1}}>
      <TouchableWithoutFeedback
        style={{width: '100%', height: '100%'}}
        onPress={() => Keyboard.dismiss}>
        <View>
          <Text style={{...globalStyles.title, color: LightTheme.colors.text}}>
            {!task ? 'New task' : 'Update task'}
          </Text>
          <TextInput
            style={styles.input}
            placeholder="Title"
            value={name}
            onChangeText={value => onChange(value, 'name')}
          />
          <TextInput
            style={{...styles.input, textAlignVertical: 'top'}}
            placeholder="Description"
            multiline
            numberOfLines={5}
            value={description}
            onChangeText={value => onChange(value, 'description')}
          />
        </View>
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            width: '100%',
          }}>
          <TouchableOpacity
            style={{
              ...styles.button,
              backgroundColor: isSubmiting
                ? 'rgba(6,106,255, 0.6)'
                : LightTheme.colors.primary,
            }}
            disabled={isSubmiting}
            onPress={submit}>
            {isSubmiting ? (
              <View style={{flexDirection: 'row'}}>
                <ActivityIndicator color="white" style={{marginRight: 10}} />
                <Text style={styles.text}>{ task ? 'Actualizando' : 'Creando' }</Text>
              </View>
            ) : (
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                }}>
                {!task ? 'Create task' : 'Update task'}
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: 'rgb(245, 248, 255)',
    marginTop: 15,
    paddingHorizontal: 10,
  },
  button: {
    height: 50,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
});
