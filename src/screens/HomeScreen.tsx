import {View, Text, SafeAreaView, RefreshControl, ActivityIndicator} from 'react-native';
import React, {useContext, useState} from 'react';
import {globalStyles, LightTheme} from '../theme/appTheme';
import {SearchInput} from '../components/SearchInput';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import {TaskCard} from '../components/TaskCard';
import {TaskContext} from '../context/task/TaskContext';
import {FloatingActionButton} from '../components/FloatingActionButton';
import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackProps} from '../navigation/Navigator';

interface Props extends StackScreenProps<RootStackProps, 'Home'> {}

export const HomeScreen = ({navigation}: Props) => {
  const {tasks, isLoading, getTasks} = useContext(TaskContext);

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator color={LightTheme.colors.primary} size={30} />
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1, flexGrow: 1}}>
      <View
        style={{
          paddingHorizontal: 0,
          flex: 1,
        }}>
        <FlatList
          data={tasks}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => navigation.navigate('Task', item)}
              activeOpacity={1}>
              <TaskCard task={item} />
            </TouchableOpacity>
          )}
          ListHeaderComponent={
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  ...globalStyles.title,
                  color: LightTheme.colors.text,
                  marginBottom: 10,
                  marginTop: 30,
                }}>
                Task list
              </Text>
              <TouchableOpacity onPress={getTasks}>
                <Icon name="reload" size={30} />
              </TouchableOpacity>
            </View>
          }
          style={{
            paddingHorizontal: 15,
          }}
        />
        <FloatingActionButton
          child={<Icon name="add" color="white" size={30} />}
          onPress={() => navigation.navigate('Task')}
        />
      </View>
    </SafeAreaView>
  );
};
