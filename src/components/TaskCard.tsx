import React, {useContext, useState} from 'react';
import {LightTheme} from '../theme/appTheme';
import {Task} from '../interfaces/appInterfaces';
import {View, Text, StyleSheet, Platform, ToastAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {TouchableOpacity} from 'react-native-gesture-handler';
import moment from 'moment'
import { TaskContext } from '../context/task/TaskContext';

interface Props {
  task: Task;
}

export const TaskCard = ({task}: Props) => {

  const [isChecked, setIsChecked] = useState( task.status === 0 );
  const { deleteTask } = useContext(TaskContext);

  const finishTask = async () => {
    await deleteTask(task.id);
    setIsChecked(true);

    if (Platform.OS === 'android') {
      ToastAndroid.show('Tarea finalizada', ToastAndroid.SHORT);
    } 
  }

  return (
    <View style={styles.bg}>
      <TouchableOpacity
        disabled={isChecked}
        onPress={finishTask}>
        {!isChecked ? (
          <Icon
            name="ellipse-outline"
            size={25}
            style={{
              marginRight: 10,
            }}
            color={LightTheme.colors.primary}
          />
        ) : (
          <Icon
            name="checkmark-circle"
            size={25}
            style={{
              marginRight: 10,
            }}
            color={LightTheme.colors.primary}
          />
        )}
      </TouchableOpacity>
      <View>
        <Text
          style={{
            ...styles.taskName,
            color: LightTheme.colors.text,
            textDecorationLine: isChecked ? "line-through" : "none"
          }}>
          {task.name}
        </Text>
        <Text>{moment(task.created_at).fromNow()}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bg: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 10,
    marginBottom: 15,
    paddingVertical: 25,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  taskName: {
    fontSize: 18,
  },
});
