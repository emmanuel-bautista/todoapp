import {TextInput, StyleSheet } from 'react-native';
import React from 'react';

interface Props {
    label: string,
    placeholder: string,
}

export const Input = () => {
    return (
        <TextInput style={styles.bg}/>
    );
};

const styles = StyleSheet.create({
  bg: {
    backgroundColor: 'rgb(245, 248, 255)',
  }
});
