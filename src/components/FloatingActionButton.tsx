import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {LightTheme} from '../theme/appTheme';

interface Props {
    child: JSX.Element,
    onPress?: () => void
}

export const FloatingActionButton = ({ child, onPress } : Props) => {
  return (
    <View
      style={{
        ...styles.fabContainer,
        backgroundColor: LightTheme.colors.primary,
      }}>
      <TouchableOpacity style={styles.fab} onPress={onPress}>
         {child}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  fabContainer: {
    width: 50,
    height: 50,
    position: 'absolute',
    bottom: 10,
    right: 10,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center'
  },
  fab: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
